'use strict';

var trainVelocity = 100;

angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'Routes', 'Distances', 'Stations',
  function ($scope, Authentication, Routes, Distances, Stations) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    $scope.init = function() {
      if ($scope.authentication.user) {
        $scope.stations = Stations.query();
        $scope.distances = Distances.query();
        $scope.routes = Routes.query();
      }
    };

    $scope.evaluateStations = function(route) {
      var result = [];
      var currentTime = new Date(route.departure);

      for (var i in route.stations) {

        if (i > 0) {
          var distance = null;
          for (var j in $scope.distances) {
            var lookupDistance = $scope.distances[j];
            if ((lookupDistance.from === route.stations[i-1]._id && lookupDistance.to === route.stations[i]._id) ||
              (lookupDistance.from === route.stations[i]._id && lookupDistance.to === route.stations[i-1]._id)) {
              distance = lookupDistance.distance;
            }
          }
          currentTime.setHours(currentTime.getHours() + distance / trainVelocity);
        }

        result.push({
          name: route.stations[i].name,
          railroads: route.stations[i].railroads,
          time: new Date(currentTime)
        });
      }
      route.evaluatedStations = result;
    };
  }
]);
