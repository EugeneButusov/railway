/**
 * Created by eugene on 07/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var stationsPolicy = require('../policies/stations.server.policy'),
  stations = require('../controllers/stations.server.controller');

module.exports = function (app) {
  /*
  Stations
   */
  app.route('/api/stations').all(stationsPolicy.isAllowed)
    .get(stations.stations.list)
    .post(stations.stations.create);

  app.route('/api/stations/:stationId').all(stationsPolicy.isAllowed)
    .get(stations.stations.read)
    .put(stations.stations.update)
    .delete(stations.stations.delete);

  app.param('stationId', stations.stations.stationByID);

  /*
   Distances
   */
  app.route('/api/distances').all(stationsPolicy.isAllowed)
    .get(stations.distances.list)
    .post(stations.distances.create);

  app.route('/api/distances/:distanceId').all(stationsPolicy.isAllowed)
    .get(stations.distances.read)
    .put(stations.distances.update)
    .delete(stations.distances.delete);

  app.param('distanceId', stations.distances.distanceByID);
};
