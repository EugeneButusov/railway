/**
 * Created by eugene on 07/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var
  path = require('path'),
  _ = require('lodash'),
  mongoose = require('mongoose'),
  Station = mongoose.model('Station'),
  Distance = mongoose.model('Distance'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/*
 Distances code
 */

exports.stations = {};

exports.stations.create = function (req, res) {
  var station = new Station(req.body);

  station.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(station);
    }
  });
};


exports.stations.read = function (req, res) {
  res.json(req.station);
};


exports.stations.update = function (req, res) {
  var station = req.station;

  station.name = req.body.name;

  station.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(station);
    }
  });
};


exports.stations.delete = function (req, res) {
  var station = req.station;

  station.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(station);
    }
  });
};

exports.stations.list = function (req, res) {
  Station.find().exec(function (err, stations) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(stations);
    }
  });
};


exports.stations.stationByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Station is invalid'
    });
  }

  Station.findById(id).exec(function (err, station) {
    if (err) {
      return next(err);
    } else if (!station) {
      return res.status(404).send({
        message: 'No station with that identifier has been found'
      });
    }
    req.station = station;
    next();
  });
};

/*
  Distances code
 */

exports.distances = {};

exports.distances.create = function (req, res) {
  var distance = new Distance(req.body);

  distance.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(distance);
    }
  });
};


exports.distances.read = function (req, res) {
  res.json(req.distance);
};


exports.distances.update = function (req, res) {
  Distance.update({ _id: req.distance._id }, { distance: req.body.distance }, function(err, value) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(req.distance);
    }
  });
};


exports.distances.delete = function (req, res) {
  var distance = req.distance;

  distance.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(distance);
    }
  });
};

exports.distances.list = function (req, res) {
  Distance.find().exec(function (err, distances) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(distances);
    }
  });
};


exports.distances.distanceByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Station is invalid'
    });
  }

  Distance.findById(id).exec(function (err, distance) {
    if (err) {
      return next(err);
    } else if (!distance) {
      return res.status(404).send({
        message: 'No distance with that identifier has been found'
      });
    }
    req.distance = distance;
    next();
  });
};
