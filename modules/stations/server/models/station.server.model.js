/**
 * Created by eugene on 07/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var StationSchema = new Schema({
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Name cannot be blank'
  },
  railroads: {
    type: Number,
    default: 1
  }
});

mongoose.model('Station', StationSchema);
