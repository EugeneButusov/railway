/**
 * Created by eugene on 07/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var DistanceSchema = new Schema({
  from: {
    type: Schema.Types.ObjectId,
    ref: 'Station',
    required: true
  },
  to: {
    type: Schema.Types.ObjectId,
    ref: 'Station',
    required: true
  },
  distance: Number
});

mongoose.model('Distance', DistanceSchema);
