/**
 * Created by eugene on 07/12/15.
 */

'use strict';

// Articles controller
angular.module('stations').controller('StationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Stations', 'Distances',
  function ($scope, $stateParams, $location, Authentication, Stations, Distances) {
    $scope.authentication = Authentication;

    $scope.findStations = function() {
      $scope.stations = Stations.query();
    };

    $scope.createStation = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'stationForm');

        return false;
      }

      // Create new Article object
      var station = new Stations({
        name: this.name,
        railroads: this.railroads
      });

      station.$save(function (response) {
        $scope.stations = Stations.query();

        // Clear form fields
        $scope.name = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.removeStation = function (station) {
      if (station) {
        station.$remove();

        for (var i in $scope.stations) {
          if ($scope.stations[i] === station) {
            $scope.stations.splice(i, 1);
          }
        }
      } else {
        $scope.station.$remove(function () {
          $location.path('stations');
        });
      }
    };

    $scope.updateStation = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'stationForm');

        return false;
      }

      var station = $scope.station;

      station.$update(function () {
        $location.path('stations/' + station._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };


    $scope.findStation = function () {
      $scope.station = Stations.get({
        stationId: $stateParams.stationId
      });
    };


    $scope.findDistances = function() {
      $scope.stations = Stations.query();
      $scope.distances = Distances.query({}, function(distances) {
        $scope.distanceValues = distances;
      });
    };

    $scope.getDistanceValue = function(from, to) {
      for (var i in $scope.distanceValues) {
        var lookupDistance = $scope.distanceValues[i];
        if ((lookupDistance.from === from._id && lookupDistance.to === to._id) ||
          (lookupDistance.from === to._id && lookupDistance.to === from._id)) {
          return lookupDistance.distance;
        }
      }
    };

    $scope.createOrUpdateDistance = function (keyupEvent, from, to) {
      $scope.error = null;

      var result = null;

      Distances.query({}, function(distances) {
        for (var i in distances) {
          var lookupDistance = distances[i];
          if ((lookupDistance.from === from._id && lookupDistance.to === to._id) ||
            (lookupDistance.from === to._id && lookupDistance.to === from._id)) {
            result = lookupDistance;
            break;
          }
        }

        if (result) {
          result.distance = keyupEvent.target.value;

          result.$update(function () {
            $location.path('distances');
          }, function (errorResponse) {
            alert(errorResponse.data.message);
          });
        } else {
          (new Distances({
            distance: keyupEvent.target.value,
            from: from,
            to: to
          })).$save(function (response) {
            $location.path('distances');
          }, function (errorResponse) {
            alert(errorResponse.data.message);
          });
        }
      });
    };

    $scope.removeStation = function (station) {
      if (station) {
        station.$remove();

        for (var i in $scope.stations) {
          if ($scope.stations[i] === station) {
            $scope.stations.splice(i, 1);
          }
        }
      } else {
        $scope.station.$remove(function () {
          $location.path('stations');
        });
      }
    };

    $scope.updateStation = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'stationForm');

        return false;
      }

      var station = $scope.station;

      station.$update(function () {
        $location.path('stations/' + station._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };


    // Find existing Article
    $scope.findStation = function () {
      $scope.station = Stations.get({
        stationId: $stateParams.stationId
      });
    };
  }
]);
