/**
 * Created by eugene on 07/12/15.
 */

'use strict';

// Setting up route
angular.module('stations').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('stations', {
        abstract: true,
        url: '/stations',
        template: '<ui-view/>'
      })
      .state('stations.list', {
        url: '',
        templateUrl: 'modules/stations/client/views/list.stations.client.view.html'
      })
      .state('stations.create', {
        url: '/create',
        templateUrl: 'modules/stations/client/views/create-station.client.view.html',
        data: {
          roles: ['dispatcher']
        }
      })
      .state('stations.view', {
        url: '/stations/:stationId',
        templateUrl: 'modules/stations/client/views/view-station.client.view.html'
      })
      .state('stations.edit', {
        url: '/stations/:stationId/edit',
        templateUrl: 'modules/stations/client/views/edit-station.client.view.html',
        data: {
          roles: ['dispatcher']
        }
      });

    $stateProvider
      .state('distances', {
        abstract: true,
        url: '/distances',
        template: '<ui-view/>'
      })
      .state('distances.list', {
        url: '',
        templateUrl: 'modules/stations/client/views/list.distances.client.view.html'
      });
  }
]);
