/**
 * Created by eugene on 07/12/15.
 */

'use strict';

// Configuring the Articles module
angular.module('stations').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Станции',
      state: 'stations',
      type: 'dropdown',
      roles: ['admin']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'stations', {
      title: 'Станции',
      state: 'stations.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'stations', {
      title: 'Перегоны',
      state: 'distances.list'
    });
  }
]);
