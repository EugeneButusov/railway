/**
 * Created by eugene on 07/12/15.
 */

'use strict';

angular.module('stations').factory('Stations', ['$resource',
  function ($resource) {
    return $resource('api/stations/:stationId', {
      stationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

angular.module('stations').factory('Distances', ['$resource',
  function ($resource) {
    return $resource('api/distances/:distanceId', {
      distanceId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
