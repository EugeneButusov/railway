/**
 * Created by eugene on 21/12/15.
 */

'use strict';

// Setting up route
angular.module('routes').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('routes', {
        abstract: true,
        url: '/routes',
        template: '<ui-view/>'
      })
      .state('routes.list', {
        url: '',
        templateUrl: 'modules/routes/client/views/list.routes.client.view.html'
      })
      .state('routes.create', {
        url: '/create',
        templateUrl: 'modules/routes/client/views/create.route.client.view.html',
        data: {
          roles: ['dispatcher']
        }
      })
      .state('routes.view', {
        url: '/stations/:stationId',
        templateUrl: 'modules/stations/client/views/view-station.client.view.html'
      })
      .state('routes.edit', {
        url: '/stations/:stationId/edit',
        templateUrl: 'modules/stations/client/views/edit-station.client.view.html',
        data: {
          roles: ['dispatcher']
        }
      });
  }
]);
