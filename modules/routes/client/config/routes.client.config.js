/**
 * Created by eugene on 21/12/15.
 */

/**
 * Created by eugene on 07/12/15.
 */

'use strict';

// Configuring the Articles module
angular.module('routes').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Сообщения',
      state: 'routes.list',
      roles: ['dispatcher']
    });
  }
]);
