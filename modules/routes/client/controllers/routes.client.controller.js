/**
 * Created by eugene on 21/12/15.
 */

'use strict';



// Articles controller
angular.module('routes').controller('RoutesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Stations', 'Routes',
  function ($scope, $stateParams, $location, Authentication, Stations, Routes) {
    $scope.authentication = Authentication;

    $scope.find = function() {
      $scope.routes = Routes.query();
    };

    $scope.prepare = function() {
      $scope.stations = [];
      $scope.stationValues = Stations.query();
    };

    $scope.create = function (isValid) {
      $scope.error = null;

      // Create new Article object
      var route = new Routes({
        departure: this.departure,
        stations: this.stations
      });

      route.$save(function (response) {
        $scope.routes = Routes.query();

        // Clear form fields
        $scope.name = '';
        $location.path('routes');
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.remove = function (route) {
      if (route) {
        route.$remove();

        for (var i in $scope.routes) {
          if ($scope.routes[i] === route) {
            $scope.routes.splice(i, 1);
          }
        }
      } else {
        $scope.route.$remove(function () {
          $location.path('routes');
        });
      }
    };

    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'routeForm');

        return false;
      }

      var route = $scope.route;

      route.$update(function () {
        $location.path('routes/' + route._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };


    $scope.findRoute = function () {
      $scope.route = Routes.get({
        routeId: $stateParams.routeId
      });
    };
  }
]);
