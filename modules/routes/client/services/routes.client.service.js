/**
 * Created by eugene on 21/12/15.
 */

'use strict';

angular.module('routes').factory('Routes', ['$resource',
  function ($resource) {
    return $resource('api/routes/:routeId', {
      routeId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
