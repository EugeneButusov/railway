/**
 * Created by eugene on 21/12/15.
 */


'use strict';

/**
 * Module dependencies.
 */
var stationsPolicy = require('../policies/routes.server.policy'),
  routes = require('../controllers/routes.server.controller');

module.exports = function (app) {
  /*
   Stations
   */
  app.route('/api/routes').all(stationsPolicy.isAllowed)
    .get(routes.list)
    .post(routes.create);

  app.route('/api/routes/:routeId').all(stationsPolicy.isAllowed)
    .get(routes.read)
    .put(routes.update)
    .delete(routes.delete);

  app.param('routeId', routes.routeByID);
};
