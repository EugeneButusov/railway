/**
 * Created by eugene on 21/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var RouteSchema = new Schema({
  departure: Date,
  stations: [{
    type: Schema.Types.ObjectId,
    ref: 'Station'
  }]
});

mongoose.model('Route', RouteSchema);
