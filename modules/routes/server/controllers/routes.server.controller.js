/**
 * Created by eugene on 21/12/15.
 */

'use strict';

/**
 * Module dependencies.
 */
var
  path = require('path'),
  _ = require('lodash'),
  mongoose = require('mongoose'),
  Station = mongoose.model('Station'),
  Route = mongoose.model('Route'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/*
 Distances code
 */

exports.create = function (req, res) {
  var route = new Route(req.body);

  if (route.stations.length < 2) {
    return res.status(400).send({
      message: 'Станций в маршруте должно быть больше 2!'
    });
  }

  route.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(route);
    }
  });
};


exports.read = function (req, res) {
  res.json(req.station);
};


exports.update = function (req, res) {
  var route = req.route;

  route.stations = req.body.stations;
  route.departure = req.body.departure;

  route.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(route);
    }
  });
};


exports.delete = function (req, res) {
  var route = req.route;

  route.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(route);
    }
  });
};

exports.list = function (req, res) {
  Route.find().populate('stations').exec(function (err, routes) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(routes);
    }
  });
};


exports.routeByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Route is invalid'
    });
  }

  Route.findById(id).exec(function (err, route) {
    if (err) {
      return next(err);
    } else if (!route) {
      return res.status(404).send({
        message: 'No route with that identifier has been found'
      });
    }
    req.route = route;
    next();
  });
};
